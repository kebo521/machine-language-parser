//================示例1，过滤文件夹下的所有文件中的 "\r" ===============================
path="../fabric/scripts/";	//定义 path 参数作用文件夹地址
list=find(path+"*.sh");	//查找../fabric/scripts/*.sh 所有sh文件
while(list)	
{//list 不为空进入 
	//1、inf(path+list) 打开path+list[0]文件,返回文件内容
	//2、将inf返回内容作为replace参数来消除"\r" -> 返回 过滤"\r"后的内容
	//3、将replace返回内容作为outf 文件输出函数的数据内容，写入到"../fabric/scripts_test/"+list[0] 文件中
	outf("../fabric/scripts_test/"+list,replace(inf(path+list),"\r"));
	list++;	//向下偏移一次 list
}

//注：更多示例 请参考 Wiki

